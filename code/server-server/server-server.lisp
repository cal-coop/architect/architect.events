#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(cl:in-package #:architect.events.build)

(define-matrix-spec (#:architect.events.server-server)
  "api/server-server/definitions/"
  "api/server-server/definitions/event-schemas/")
