(asdf:defsystem #:architect.events.server-server
  :description "generated classes for the matrix server-server spec"
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :license  "Artistic License 2.0"
  :version "0.0.1"
  :depends-on ("architect.events.util")
  :serial t
  :components ((:file "server-server")))
