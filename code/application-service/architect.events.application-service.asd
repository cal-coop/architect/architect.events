(asdf:defsystem #:architect.events.application-service
  :description "generated classes for the matrix application-service spec"
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :license  "Artistic License 2.0"
  :version "0.0.1"
  :depends-on ("architect.events.util")
  :serial t
  :components ((:file "application-service")))
