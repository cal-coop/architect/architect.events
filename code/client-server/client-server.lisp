#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(cl:in-package #:architect.events.build)

(define-matrix-spec (#:architect.events.client-server)
  "api/client-server/definitions/event-schemas/schema/core-event-schema/"
  "api/client-server/definitions/event-schemas/schema/"
  "api/client-server/definitions/"
  "api/client-server/definitions/wellknown/"
  "api/client-server/definitions/errors/"
  )
