#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:architect.events.util
  (:use #:cl)
  (:local-nicknames (#:js :json-schema2))
  (:export
   #:define-matrix-spec))

(in-package #:architect.events.util)

(defmacro define-matrix-spec ((package-designator &rest options) &rest spec-dirs)
  `(js:define-schema-spec (,package-designator ,@options)
       ,@ (loop :for dir :in spec-dirs
             :collect (architect.matrix-spec:spec dir))))

(defpackage #:architect.events.build
  (:use #:architect.events.util))
