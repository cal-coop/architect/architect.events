(asdf:defsystem #:architect.events.util
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :license  "Artistic License 2.0"
  :depends-on ("json-schema2" "architect.matrix-spec")
  :serial t
  :components ((:file "util")))
