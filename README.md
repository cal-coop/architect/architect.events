# architect.events

## what's this?

This repository contains serializable classes generated from the matrix specification using [json-clos](https://gitlab.com/Gnuxie/json-clos) and [json-schema2](https://gitlab.com/Gnuxie/json-schema2).

It's similar to [Ruma Events](https://github.com/ruma/ruma-events) except all the classes are generated dynamically and given to the compiler after macro-expansion.

The code in this repository (20 lines?) is a small amount of bureaucracy that handles json-schema2 to make the classes available as asdf systems.

It is not 100% complete as json-schema2 is not complete, as more features of json-schema get supported there they will be available here.

## features

We provide  `architect.events.client-server`, `architect.events.server-server` and `architect.events.application-service`. 

The other functionality comes from json-clos which can be made to work with any serializer.

## installation

this isn't on quicklisp yet and neither are it's dependencies, you will need the following in your local-projects.

```
cd ~/quicklisp/local-projects
git clone https://gitlab.com/cal-coop/architect/architect.matrix-spec.git
git clone https://gitlab.com/Gnuxie/json-clos.git
git clone https://gitlab.com/Gnuxie/json-schema2.git
git clone https://gitlab.com/cal-coop/architect/architect.events.git
```
then you can just `(ql:quickload :architect.events.client-server)`.
